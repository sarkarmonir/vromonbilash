<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ isset($title) ? $title : 'no title' }}</title>
    <link rel="shortcut icon" href="{{ url('assets/web/images/vromon-logo.png') }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ url('assets/web/css/style.css') }}">
    <link rel="stylesheet" href="{{ url('assets/web/vendors/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/web/vendors/css/animate.min.css') }}">
    <!-- fontawesome -->
    <script defer src="{{ url('assets/web/js/fa.js') }}"></script>
</head>
<body>

{{-- header --}}
@include('web.partials.header')

{{-- contant --}}
@yield('content')

{{-- footer --}}
@include('web.partials.footer')
  
  <!-- script -->
  <script src="{{ url('assets/web/vendors/jquery/jquery.min.js') }}"></script>
  <script src="{{ url('assets/web/js/parallax.min.js') }}"></script>
  <script src="{{ url('assets/web/js/main.js') }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
  <script src="{{ url('assets/web/vendors/js/bootstrap.min.js') }}"></script>
  <script src="{{ url('assets/web/js/jquery.sticky.js') }}"></script>
  <script src="{{ url('assets/web/js/wow.min.js') }}"></script>
  <script>
    $(document).ready(function(){

        new WOW().init();

        $('.carousel').carousel();

        $(".sticker").sticky({topSpacing:0});

        $('.parallax-window').parallax({imageSrc: '{{ url("assets/web/images/slider/7.jpg") }}'});
        $('.parallax-window2').parallax({imageSrc: '{{ url("assets/web/images/slider/7.jpg") }}'});

        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });

        $('.scrollup').click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });
        $('#myTab a').on('click', function (e) {
            e.preventDefault()
            $(this).tab('show')
        })
        
    });
  </script>



</body>
</html>