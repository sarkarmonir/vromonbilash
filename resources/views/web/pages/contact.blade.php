@extends('web.layout.layout')
@section('content')
@include('web.partials.sub-page-header')
    <section class="mb-5 mt-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 bn">
                    <h4>
                        ভ্রমন বিলাশ যোগাযোগ
                    </h4>
                    <address>
                        ঠিকানা: বাড়ি # 12 (ইসলামী ব্যাংক এর ডান পাশের নিকটবর্তী স্কুল অ্যান্ড কলেজ, মিরপুর), রোড # 7 পূর্ব-পশ্চিম স্কুল রোড, ব্লক # এফ, মিরপুর -২, ঢাকা, বাংলাদেশ
                    </address>
                        
                        ফোন: +88 01911-920666 <br>
                        +88 01818-969222,<br>
                        +88 01711-947931,<br>
                        +88 01818789168<br><br>
                        
                        ইমেল: info@vromonbilash.com,<br>
                        
                        jewelrana.nc@gmail.com<br>
                        
                        বুকিং: book@vromonbilash.com
                </div>
                <div class="col-lg-6">
                    <form>
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="email" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Enter Full Name">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="email" class="form-control" id="phone" aria-describedby="emailHelp" placeholder="Enter phone">
                        </div>
                        <div class="form-group">
                            <label for="message">Message</label>
                            <textarea name="" id="" cols="30" rows="6" class="form-control" id="message" ></textarea>
                            
                        </div>
                        <button type="submit" class="btn btn-primary">Send</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    

@endsection