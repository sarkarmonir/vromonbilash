@extends('web.layout.layout')
@section('content')
@include('web.partials.sub-page-header')
    <section class="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="service wow zoomIn">
                        <img src="{{ url('assets/web/images/r1.jpg') }}" alt="">
                        <div class="over">
                            <h1>Resort 01</h1>
                            <p>Omnis explicabo labore eligendi error provident beatae</p>
                            <a href="#" class="btn btn-info">read more</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                        <div class="service wow zoomIn">
                            <img src="{{ url('assets/web/images/r1.jpg') }}" alt="">
                            <div class="over">
                                <h1>Resort 01</h1>
                                <p>Omnis explicabo labore eligendi error provident beatae</p>
                                <a href="#" class="btn btn-info">read more</a>
                            </div>
                        </div>
                </div>
                <div class="col-lg-3">
                        <div class="service wow zoomIn">
                            <img src="{{ url('assets/web/images/r1.jpg') }}" alt="">
                            <div class="over">
                                <h1>Resort 01</h1>
                                <p>Omnis explicabo labore eligendi error provident beatae</p>
                                <a href="#" class="btn btn-info">read more</a>
                            </div>
                        </div>
                </div>
                <div class="col-lg-3">
                        <div class="service wow zoomIn">
                            <img src="{{ url('assets/web/images/r1.jpg') }}" alt="">
                            <div class="over">
                                <h1>Resort 01</h1>
                                <p>Omnis explicabo labore eligendi error provident beatae</p>
                                <a href="#" class="btn btn-info">read more</a>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </section>
    

@endsection