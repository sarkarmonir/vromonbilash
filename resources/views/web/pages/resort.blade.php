@extends('web.layout.layout')
@section('content')
@include('web.partials.sub-page-header')
<section class="kazi-office">
    <div class="container">
        {{-- <h2 class="text-center">Resorts</h2> --}}

        <div class="row">
            <div class="col-lg-3">
                <div class="card wow zoomIn">
                        <a href="{{ url('resortdetails') }}"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort4.jpg') }}" alt="Card image cap"></a>
                        <div class="card-body">
                            <a href="{{ url('resortdetails') }}"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                            <p class="address">
                            <i class="fa fa-map-marker-alt"></i> 
                            13/A, Sonargaon Road, Banglamotor, Dhaka
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card wow zoomIn">
                        <a href="{{ url('resortdetails') }}"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort1.jpg') }}" alt="Card image cap"></a>
                        <div class="card-body">
                            <a href="{{ url('resortdetails') }}"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                            <p class="address">
                            <i class="fa fa-map-marker-alt"></i> 
                            13/A, Sonargaon Road, Banglamotor, Dhaka
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card wow zoomIn">
                        <a href="{{ url('resortdetails') }}"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort5.jpg') }}" alt="Card image cap"></a>
                        <div class="card-body">
                            <a href="{{ url('resortdetails') }}"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                            <p class="address">
                            <i class="fa fa-map-marker-alt"></i> 
                            13/A, Sonargaon Road, Banglamotor, Dhaka
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card wow zoomIn">
                        <a href="{{ url('resortdetails') }}"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort2.jpg') }}" alt="Card image cap"></a>
                        <div class="card-body">
                            <a href="{{ url('resortdetails') }}"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                            <p class="address">
                            <i class="fa fa-map-marker-alt"></i> 
                            13/A, Sonargaon Road, Banglamotor, Dhaka
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-3">
                <div class="card wow zoomIn">
                        <a href="{{ url('resortdetails') }}"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort4.jpg') }}" alt="Card image cap"></a>
                        <div class="card-body">
                            <a href="{{ url('resortdetails') }}"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                            <p class="address">
                            <i class="fa fa-map-marker-alt"></i> 
                            13/A, Sonargaon Road, Banglamotor, Dhaka
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card wow zoomIn">
                        <a href="{{ url('resortdetails') }}"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort1.jpg') }}" alt="Card image cap"></a>
                        <div class="card-body">
                            <a href="{{ url('resortdetails') }}"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                            <p class="address">
                            <i class="fa fa-map-marker-alt"></i> 
                            13/A, Sonargaon Road, Banglamotor, Dhaka
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card wow zoomIn">
                        <a href="{{ url('resortdetails') }}"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort5.jpg') }}" alt="Card image cap"></a>
                        <div class="card-body">
                            <a href="{{ url('resortdetails') }}"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                            <p class="address">
                            <i class="fa fa-map-marker-alt"></i> 
                            13/A, Sonargaon Road, Banglamotor, Dhaka
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card wow zoomIn">
                        <a href="{{ url('resortdetails') }}"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort2.jpg') }}" alt="Card image cap"></a>
                        <div class="card-body">
                            <a href="{{ url('resortdetails') }}"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                            <p class="address">
                            <i class="fa fa-map-marker-alt"></i> 
                            13/A, Sonargaon Road, Banglamotor, Dhaka
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-3">
                <div class="card wow zoomIn">
                        <a href="{{ url('resortdetails') }}"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort4.jpg') }}" alt="Card image cap"></a>
                        <div class="card-body">
                            <a href="{{ url('resortdetails') }}"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                            <p class="address">
                            <i class="fa fa-map-marker-alt"></i> 
                            13/A, Sonargaon Road, Banglamotor, Dhaka
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card wow zoomIn">
                        <a href="{{ url('resortdetails') }}"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort1.jpg') }}" alt="Card image cap"></a>
                        <div class="card-body">
                            <a href="{{ url('resortdetails') }}"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                            <p class="address">
                            <i class="fa fa-map-marker-alt"></i> 
                            13/A, Sonargaon Road, Banglamotor, Dhaka
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card wow zoomIn">
                        <a href="{{ url('resortdetails') }}"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort5.jpg') }}" alt="Card image cap"></a>
                        <div class="card-body">
                            <a href="{{ url('resortdetails') }}"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                            <p class="address">
                            <i class="fa fa-map-marker-alt"></i> 
                            13/A, Sonargaon Road, Banglamotor, Dhaka
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card wow zoomIn">
                        <a href="{{ url('resortdetails') }}"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort2.jpg') }}" alt="Card image cap"></a>
                        <div class="card-body">
                            <a href="{{ url('resortdetails') }}"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                            <p class="address">
                            <i class="fa fa-map-marker-alt"></i> 
                            13/A, Sonargaon Road, Banglamotor, Dhaka
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection