@extends('web.layout.layout')
@section('content')
@include('web.partials.sub-page-header')
<section class="details-body">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="profile-pic">
                    <img src="{{ url('assets/web/images/favicon.png')}}" alt="">
                </div>
                <div class="left-nav">
                    <h3 class="profile-name d-n">Somudro Bilash <span><i class="fas fa-check-circle"></i></span>
                        <hr>
                    </h3>
                        <div class="rating">
                            Rating: 
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <hr>
                        <p class="contact-no">
                            <i class="fas fa-phone"></i> 0123 456 789
                        </p>
                        <hr>
                        <div class="business-hr">
                            <i class="fas fa-clock"></i> Business Hour <br>
                            <table width="100%">
                                    <tr>
                                        <td width="50%">Sun - Thu</td>
                                        <td width="50%">: 10AM - 08PM</td>
                                    </tr>
                                    <tr>
                                        <td>Fri - Sat</td>
                                        <td>: 03PM - 08PM</td>
                                    </tr>
                            </table>
                        </div>
                        <hr>
                        <div class="contact-info">
                            <p class="address">
                                <i class="fa fa-map-marker-alt"></i> Address <br>
                                13/A, Sonargaon Road, Banglamotor, Dhaka
                            </p>
                        </div>
                        
                        <div class="link">
                                <ul>
                                    <li><a href="#"><i class="fas fa-link"></i></a></li>
                                    <li><a href="#"><i class="fab fa-facebook-square"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                    </div>
            </div>
            
            <div class="col-lg-9">
                    <h3 class="profile-name d-b">Somudro Bilash <span><i class="fas fa-check-circle"></i></span></h3>
                    <div class="res-subnav">
                        <i class="fas fa-bars"></i>
                    </div>
                    <div class="sub-nav">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li><a class="active" href="#home"><i class="fas fa-home"></i></a></li>
                            <li><a class="" href="#rooms">Rooms</a></li>
                            <li><a class="" href="#gallery">Gallery</a></li>
                            <li><a class="" href="#pricing">Pricing</a></li>
                            <li><a class="" href="#offers">Offers</a></li>
                            <li><a class="" href="#facilities">Facilities</a></li>
                            <li><a class="" href="#review">Review</a></li>
                            <li><a class="" href="#location">Location map</a></li>
                        </ul>
                    </div>
                    <div class="sub-body">
                            <div class="sub-body-contant">
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="home" role="tabpanel">
                                        <img width="100%" src="{{ url('assets/web/images/slider/9.jpg')}}" alt="">
                                        <h3>Welcome our resort</h3>
                                        <p>
                                            <span>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ad minima, non necessitatibus, fugiat reprehenderit sunt obcaecati, animi iure quia autem nemo et eveniet quae mollitia placeat earum temporibus at esse!</span>
                                            <span>Voluptas sunt eveniet, adipisci quia expedita id exercitationem, cumque ad officiis deleniti animi ea. Sed dignissimos accusamus amet dolorum enim, iste voluptates nam, suscipit similique alias rem soluta totam. Corporis.</span>
                                            <span>Saepe itaque, dolorem expedita labore laboriosam asperiores quidem cumque veniam omnis soluta accusamus consequuntur illum et hic explicabo voluptate sunt dolorum laborum eligendi rem aut? In officia asperiores corporis eos!</span>
                                        </p>
                                    </div>
                                    <div class="tab-pane fade" id="rooms" role="tabpanel">
                                        <div class="rooms">
                                            <ul>
                                                <li>
                                                    <img src="{{ url('assets/web/images/room4.jpg')}}" alt="">
                                                    <h3>STANDARD ROOM</h3>
                                                    <p>They are -Heritage, Terrace, Tower Cottage and Queen Size.</p>
                                                    <small>
                                                            1. Air Conditioning
                                                            2. Hot & Cool Shower(Hot/Cold Water) 
                                                            3. Mini Refrigerator* Flat Screen TV
                                                            4. Satellite/Cable TV
                                                            5. Work Desk
                                                            6. Telephone
                                                            7. Wake-Up Service
                                                            8. Slippers
                                                            9. Towels/Linen
                                                            10 Balcony
                                                            11 Safety locker
                                                    </small>
                                                    <h4>BDT 5500++ /per night</h4>
                                                </li>
                                                
                                                <li>
                                                        <img src="{{ url('assets/web/images/room3.jpg')}}" alt="">
                                                        <h3>DELUXE ROOM MUD HOUSE</h3>
                                                        <p>They are -Heritage, Terrace, Tower Cottage and Queen Size.</p>
                                                        <small>
                                                                1. Air Conditioning
                                                                2. Hot & Cool Shower(Hot/Cold Water) 
                                                                3. Mini Refrigerator* Flat Screen TV
                                                                4. Satellite/Cable TV
                                                                5. Work Desk
                                                                6. Telephone
                                                                7. Wake-Up Service
                                                                8. Slippers
                                                                9. Towels/Linen
                                                                10 Balcony
                                                                11 Safety locker
                                                        </small>
                                                        <h4>BDT 7500++ /per night</h4>
                                                    </li>
                                                    <li>
                                                            <img src="{{ url('assets/web/images/room2.jpg')}}" alt="">
                                                            <h3>DELUXE TWIN BED</h3>
                                                            <p>They are -Heritage, Terrace, Tower Cottage and Queen Size.</p>
                                                            <small>
                                                                    1. Air Conditioning
                                                                    2. Hot & Cool Shower(Hot/Cold Water) 
                                                                    3. Mini Refrigerator* Flat Screen TV
                                                                    4. Satellite/Cable TV
                                                                    5. Work Desk
                                                                    6. Telephone
                                                                    7. Wake-Up Service
                                                                    8. Slippers
                                                                    9. Towels/Linen
                                                                    10 Balcony
                                                                    11 Safety locker
                                                            </small>
                                                            <h4>BDT 8500++ /per night</h4>
                                                        </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="gallery" role="tabpanel">
                                        <h3>Gallery</h3>
                                        <div class="gallery-wrapper">
                                            <a class="galpop-info" data-galpop-group="info" data-galpop-link="#" data-galpop-link-title="Info" data-galpop-link-target="_blank" title="An apocalyptic Earth." href="assets/web/images/slider/8.jpg">
                                                <img src="{{ url('assets/web/images/slider/8.jpg')}}" alt="An apocalyptic Earth." />
                                            </a>
                                            <a class="galpop-info" data-galpop-group="info" data-galpop-link="#" data-galpop-link-title="Info" data-galpop-link-target="_blank" title="An apocalyptic Earth." href="assets/web/images/slider/8.jpg">
                                                <img src="{{ url('assets/web/images/slider/8.jpg')}}" alt="An apocalyptic Earth." />
                                            </a>
                                            <a class="galpop-info" data-galpop-group="info" data-galpop-link="#" data-galpop-link-title="Info" data-galpop-link-target="_blank" title="An apocalyptic Earth." href="assets/web/images/slider/8.jpg">
                                                <img src="{{ url('assets/web/images/slider/8.jpg')}}" alt="An apocalyptic Earth." />
                                            </a>
                                            <a class="galpop-info" data-galpop-group="info" data-galpop-link="#" data-galpop-link-title="Info" data-galpop-link-target="_blank" title="An apocalyptic Earth." href="assets/web/images/slider/8.jpg">
                                                <img src="{{ url('assets/web/images/slider/8.jpg')}}" alt="An apocalyptic Earth." />
                                            </a>
                                            <a class="galpop-info" data-galpop-group="info" data-galpop-link="#" data-galpop-link-title="Info" data-galpop-link-target="_blank" title="An apocalyptic Earth." href="assets/web/images/slider/8.jpg">
                                                <img src="{{ url('assets/web/images/slider/8.jpg')}}" alt="An apocalyptic Earth." />
                                            </a>
                                            <a class="galpop-info" data-galpop-group="info" data-galpop-link="#" data-galpop-link-title="Info" data-galpop-link-target="_blank" title="An apocalyptic Earth." href="assets/web/images/slider/8.jpg">
                                                <img src="{{ url('assets/web/images/slider/8.jpg')}}" alt="An apocalyptic Earth." />
                                            </a>
                                            <a class="galpop-info" data-galpop-group="info" data-galpop-link="#" data-galpop-link-title="Info" data-galpop-link-target="_blank" title="An apocalyptic Earth." href="assets/web/images/slider/8.jpg">
                                                <img src="{{ url('assets/web/images/slider/8.jpg')}}" alt="An apocalyptic Earth." />
                                            </a>
                                            <a class="galpop-info" data-galpop-group="info" data-galpop-link="#" data-galpop-link-title="Info" data-galpop-link-target="_blank" title="An apocalyptic Earth." href="assets/web/images/slider/8.jpg">
                                                <img src="{{ url('assets/web/images/slider/8.jpg')}}" alt="An apocalyptic Earth." />
                                            </a>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pricing" role="tabpanel">
                                        <h3>Pricing</h3>
                                        <ul>
                                            <li>Packages 01 : Price 5000</li>
                                            <li>Packages 02 : Price 15000</li>
                                            <li>Packages 03 : Price 20000</li>
                                        </ul>
                                    </div>
                                    <div class="tab-pane fade" id="offers" role="tabpanel">
                                        <h3>Offers</h3>
                                        <ul>
                                            <li>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ipsum eius laudantium, enim obcaecati saepe optio nisi quibusdam. Mollitia adipisci dolores sit, expedita modi est excepturi? Omnis architecto repudiandae consectetur aspernatur.</li>
                                            <li>Velit nostrum rerum voluptate at quasi distinctio ut nesciunt minus, dignissimos odit consequuntur aut quis? Repellendus, enim iste exercitationem placeat, quo totam vel sunt provident recusandae sequi distinctio sint. Distinctio.</li>
                                            <li>Excepturi incidunt eos rerum, illo delectus minima aliquam facilis rem doloribus aperiam magnam quos saepe maiores laudantium dolore alias! Nostrum temporibus sequi incidunt nesciunt exercitationem, debitis architecto? Perferendis, voluptatem reiciendis?</li>
                                        </ul>
                                    </div>
                                    <div class="tab-pane fade" id="review" role="tabpanel">Coming soon...</div>
                                    <div class="tab-pane fade" id="facilities" role="tabpanel">
                                        <h3>Contact info</h3>
                                        <p class="address">
                                            <i class="fa fa-map-marker-alt"></i> Address <br>
                                            13/A, Sonargaon Road, Banglamotor, Dhaka
                                        </p>
                                    </div>
                                    <div class="tab-pane fade" id="location" role="tabpanel">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d5164.79333594618!2d90.3948534516599!3d23.74522575943947!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1522494390694" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
        </div>
    </div>
</section>

@endsection