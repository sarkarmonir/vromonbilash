@extends('web.layout.layout')

@section('content')

    {{-- slider --}}
    @include('web.partials.slider')

    <section class="services">
        <div class="container">
            <div class="row bn">
                    <div class="col-lg-2">
                        <div class="sector">
                            <img src="{{ url('assets/web/images/sector/resort.png') }}" alt="">
                            <h2>
                                রিসোর্ট 
                            </h2>
                            <p>
                                বাংলাদেশের সমস্ত রিসোর্ট খোঁজ করুন
                            </p>
                                
                            <a href="#" class="btn btn-outline-success">বিস্তারিত</a>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="sector">
                            <img src="{{ url('assets/web/images/sector/park.png') }}" alt="">
                            <h2>

                                পার্ক
                            </h2>
                            <p>

                                বাংলাদেশের সমস্ত পার্ক ঘুরে দেখুন
                            </p>
                            <a href="#" class="btn btn-outline-success">বিস্তারিত</a>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="sector">
                            <img src="{{ url('assets/web/images/sector/film.png') }}" alt="">
                            <h2>    শুটিং স্পট
                            </h2>
                            <p>
                                বাংলাদেশের সমস্ত শুটিং স্পট খোঁজ করুন
                            </p>
                            <a href="#" class="btn btn-outline-success">বিস্তারিত</a>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="sector">
                            <img src="{{ url('assets/web/images/sector/old.png') }}" alt="">
                            <h2>
                                পুরানো হেরিটেজ
                            </h2>
                            <p>
                                বাংলাদেশের সমস্ত ওল্ড হেরিটেজ খোঁজ করুন
                            </p>
                            <a href="#" class="btn btn-outline-success">বিস্তারিত</a>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="sector">
                            <img src="{{ url('assets/web/images/sector/picnic.png') }}" alt="">
                            <h2>
                                পিকনিক স্পট
                            </h2>
                            <p>
                                বাংলাদেশের সমস্ত পিকনিক স্পট খোঁজ করুন
                            </p>
                            <a href="#" class="btn btn-outline-success">বিস্তারিত</a>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="sector">
                            <img src="{{ url('assets/web/images/sector/others.png') }}" alt="">
                            <h2>
                                অন্যান্য স্পট
                            </h2>
                            <p>
                                বাংলাদেশের সমস্ত অন্যান্য অঞ্চল ঘুরে দেখুন
                            </p>
                            <a href="#" class="btn btn-outline-success">বিস্তারিত</a>
                        </div>
                    </div>
            </div>
        </div>
    </section>
    
    <section class="kazi-office">
        <div class="container">
            <h2 class="text-center">Featured Resorts</h2>
            <div class="row">
                <div class="col-lg-3">
                    <div class="card wow zoomIn">
                            <a href="resort-details.html"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort4.jpg') }}" alt="Card image cap"></a>
                            <div class="card-body">
                                <a href="resort-details.html"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                                <p class="address">
                                <i class="fa fa-map-marker-alt"></i> 
                                13/A, Sonargaon Road, Banglamotor, Dhaka
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card wow zoomIn">
                            <a href="resort-details.html"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort1.jpg') }}" alt="Card image cap"></a>
                            <div class="card-body">
                                <a href="resort-details.html"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                                <p class="address">
                                <i class="fa fa-map-marker-alt"></i> 
                                13/A, Sonargaon Road, Banglamotor, Dhaka
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card wow zoomIn">
                            <a href="resort-details.html"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort5.jpg') }}" alt="Card image cap"></a>
                            <div class="card-body">
                                <a href="resort-details.html"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                                <p class="address">
                                <i class="fa fa-map-marker-alt"></i> 
                                13/A, Sonargaon Road, Banglamotor, Dhaka
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card wow zoomIn">
                            <a href="resort-details.html"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort2.jpg') }}" alt="Card image cap"></a>
                            <div class="card-body">
                                <a href="resort-details.html"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                                <p class="address">
                                <i class="fa fa-map-marker-alt"></i> 
                                13/A, Sonargaon Road, Banglamotor, Dhaka
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="kazi parallax-window2">
        <div class="container">
            <h2 class="text-center">Featured Park</h2>
            <div class="row">
                <div class="col-lg-3">
                    <div class="card wow zoomIn">
                            <a href="resort-details.html"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort4.jpg') }}" alt="Card image cap"></a>
                            <div class="card-body">
                                <a href="resort-details.html"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                                <p class="address">
                                <i class="fa fa-map-marker-alt"></i> 
                                13/A, Sonargaon Road, Banglamotor, Dhaka
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card wow zoomIn">
                            <a href="resort-details.html"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort1.jpg') }}" alt="Card image cap"></a>
                            <div class="card-body">
                                <a href="resort-details.html"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                                <p class="address">
                                <i class="fa fa-map-marker-alt"></i> 
                                13/A, Sonargaon Road, Banglamotor, Dhaka
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card wow zoomIn">
                            <a href="resort-details.html"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort5.jpg') }}" alt="Card image cap"></a>
                            <div class="card-body">
                                <a href="resort-details.html"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                                <p class="address">
                                <i class="fa fa-map-marker-alt"></i> 
                                13/A, Sonargaon Road, Banglamotor, Dhaka
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card wow zoomIn">
                            <a href="resort-details.html"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort2.jpg') }}" alt="Card image cap"></a>
                            <div class="card-body">
                                <a href="resort-details.html"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                                <p class="address">
                                <i class="fa fa-map-marker-alt"></i> 
                                13/A, Sonargaon Road, Banglamotor, Dhaka
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="kazi-office">
        <div class="container">
            <h2 class="text-center">Featured Spot</h2>
            <div class="row">
                <div class="col-lg-3">
                    <div class="card wow zoomIn">
                            <a href="resort-details.html"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort4.jpg') }}" alt="Card image cap"></a>
                            <div class="card-body">
                                <a href="resort-details.html"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                                <p class="address">
                                <i class="fa fa-map-marker-alt"></i> 
                                13/A, Sonargaon Road, Banglamotor, Dhaka
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card wow zoomIn">
                            <a href="resort-details.html"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort1.jpg') }}" alt="Card image cap"></a>
                            <div class="card-body">
                                <a href="resort-details.html"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                                <p class="address">
                                <i class="fa fa-map-marker-alt"></i> 
                                13/A, Sonargaon Road, Banglamotor, Dhaka
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card wow zoomIn">
                            <a href="resort-details.html"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort5.jpg') }}" alt="Card image cap"></a>
                            <div class="card-body">
                                <a href="resort-details.html"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                                <p class="address">
                                <i class="fa fa-map-marker-alt"></i> 
                                13/A, Sonargaon Road, Banglamotor, Dhaka
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card wow zoomIn">
                            <a href="resort-details.html"><img class="card-img-top" src="{{ url('assets/web/images/resort/resort2.jpg') }}" alt="Card image cap"></a>
                            <div class="card-body">
                                <a href="resort-details.html"><h5 class="card-title">Ruposhi Bangla Resort</h5></a>
                                <p class="address">
                                <i class="fa fa-map-marker-alt"></i> 
                                13/A, Sonargaon Road, Banglamotor, Dhaka
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="brand-slider">
        <div class="container">
            <div class="row">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="row">
                    
                        <div class="col-lg-1"></div>
                        <div class="col-lg-2">
                                <img class="" src="{{ url('assets/web/images/prt1.png') }}" alt="Second slide">
                                </div>
                                <div class="col-lg-2">
                                <img class="" src="{{ url('assets/web/images/prt2.png') }}" alt="Second slide">
                                </div>
                                <div class="col-lg-2">
                                <img class="" src="{{ url('assets/web/images/prt3.png') }}" alt="Second slide">
                                </div>
                                <div class="col-lg-2">
                                <img class="" src="{{ url('assets/web/images/prt4.png') }}" alt="Second slide">
                                </div>
                                <div class="col-lg-2">
                                <img class="" src="{{ url('assets/web/images/prt5.png') }}" alt="Second slide">
                        </div>
                        <div class="col-lg-1"></div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-2">
                                <img class="" src="{{ url('assets/web/images/prt1.png') }}" alt="Second slide">
                                </div>
                                <div class="col-lg-2">
                                <img class="" src="{{ url('assets/web/images/prt2.png') }}" alt="Second slide">
                                </div>
                                <div class="col-lg-2">
                                <img class="" src="{{ url('assets/web/images/prt3.png') }}" alt="Second slide">
                                </div>
                                <div class="col-lg-2">
                                <img class="" src="{{ url('assets/web/images/prt4.png') }}" alt="Second slide">
                                </div>
                                <div class="col-lg-2">
                                <img class="" src="{{ url('assets/web/images/prt5.png') }}" alt="Second slide">
                            </div>
                            <div class="col-lg-1"></div>
                        </div>
                    </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>             
            </div>
        </div>
    </section>

    <section class="q-contact sucscribe bn">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h4 class="text-center">সর্বশেষ তথ্য পেতে - আজই আমাদের নিউজলেটার সাবস্ক্রাইব করুন</h4>
                    <br>
                    <form>
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-3">
                            <div class="form-group">
                                <input type="text" class="form-control" id="name" aria-describedby="nameHelp" placeholder="Enter your name">
                            </div>
                            </div>
                            <div class="col-lg-3">
                            <div class="form-group">
                                <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter your email">
                            </div>
                            </div>
                            <div class="col-lg-2">
                                <button type="submit" class="btn btn-danger btn-block btn-search">সাবস্ক্রাইব</button>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    
@endsection