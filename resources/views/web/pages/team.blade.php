@extends('web.layout.layout')
@section('content')
@include('web.partials.sub-page-header')
<section class="kazi-office">
    <div class="container">
        {{-- <div class="row">
            <div class="col-lg-12 bn text-center">
                <h4>
                    আমাদের টিম
                </h4>
            </div>
        </div> --}}
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="card text-center mb-5">
                    <img src="{{ url('assets/web/images/team/jewel.jpg') }}" alt="">
                    <div class="card-body bn">
                        <h4>মোহাম্মদ জুয়েল রানা</h4>
                        <p>ব্যবস্থাপনা পরিচালক</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="card text-center mb-5">
                    <img src="{{ url('assets/web/images/team/rani.jpg') }}" alt="">
                    <div class="card-body bn">
                        <h4>দেবী রানি</h4>
                        <p>চিফ এক্সিকিউটিভ ডিরেক্টর</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="card text-center mb-5">
                    <img src="{{ url('assets/web/images/team/sadia.jpg') }}" alt="">
                    <div class="card-body bn">
                        <h4>সাদিয়া রাইসা তাসিন</h4>
                        <p>নির্বাহী পরিচালক</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="card text-center mb-5">
                    <img src="{{ url('assets/web/images/team/ripon.jpg') }}" alt="">
                    <div class="card-body bn">
                        <h4>আসাদুজ্জামান রিপন</h4>
                        <p>মার্কেটিং ব্যবস্থাপক</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="card text-center mb-5">
                    <img src="{{ url('assets/web/images/team/bappi.jpg') }}" alt="">
                    <div class="card-body bn">
                        <h4>মনজুর বাপ্পি</h4>
                        <p>আইটি বিশেষজ্ঞ</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection