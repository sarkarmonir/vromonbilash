<section class="slider">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="{{ url('assets/web/images/slider/s7.jpg') }}" alt="slide">
                <div class="carousel-caption d-none d-md-block">
                    <h3>Title</h3>
                    <p>Description</p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ url('assets/web/images/slider/s2.jpg') }}" alt="slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ url('assets/web/images/slider/s3.jpg') }}" alt="slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ url('assets/web/images/slider/s4.jpg') }}" alt="slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ url('assets/web/images/slider/s5.jpg') }}" alt="slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ url('assets/web/images/slider/s6.jpg') }}" alt="slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ url('assets/web/images/slider/s1.jpg') }}" alt="slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ url('assets/web/images/slider/s8.jpg') }}" alt="slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>


<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-center mt-5 mb-5 bn">
                <h4>বাংলা সাহিত্যের সম্রাট কিংবদন্তী লেখক হুমায়ূন আহমেদের <br>
                নুহাশ পল্লী ও দারুচিনি দ্বীপের “সমুদ্র বিলাসে ” আপনাকে স্বাগতম ।</h4>
                <p>
                    নুহাশ পল্লী বাংলা সাহিত্যের সবচেয়ে জনপ্রিয় ও কিংবদন্তি লেখক হুমায়ূন আহমেদের নন্দন কানন ।এটি হুমায়ূন আহমেদের নিজস্ব শুটিং স্পট এবং তাঁর দ্বিতীয় বাসস্থান। নুহাশ পল্লী কে নিয়ে নতুন করে বলার কিছু নেই, হুমায়ূন আহমেদ ঢাকার খুব কাছেই গাজীপুর জেলার সদর উপজেলার পিরুজালি গ্রামে প্রতিষ্ঠা করেছেন তার প্রিয় এই নুহাশ পল্লী ।প্রিয় লেখকের প্রাকৃতিক সৌন্দর্যের অপরুপ এই লীলা ভূমি দেখলে মনে হবে বিশাল গজারি বনের মাঝখানে প্রকৃতি যেন একটা সুন্দর সবুজ প্রশান্তির চাদর বিছিয়ে রেখেছে যা দেখার জন্য বাংলাদেশ সহ পৃথিবীর বিভিন্ন দেশ থেকে পর্যটকরা আসেন। বর্তমানে নুহাশ পল্লী বাংলাদেশের মানুষের কাছে পিকনিকের অন্যতম আকর্ষণীয় স্থান হিসাবে গুরুত্ব পাচ্ছে। কিন্তু অনেকে জানেন না কিভাবে নুহাশ পল্লী যাবেন? কোথায় খাবেন কি কি দেখবেন ? কিভাবে পিকনিক করবেন? বিস্তারিত উত্তর জানতে ডায়াল করুন +৮৮০১৯১১৯২০৬৬৬, +৮৮০১৮১৮৯৬৯২২২
                </p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="nuhash">
                <br>
                <h5 class="bn">এখনই বুক করুন নুহাশ পল্লী <i class="fas fa-hand-point-right"></i></h5>
                <img width="100%" src="{{ url('assets/web/images/nuhash.jpg') }}" alt="">
            </div>
        </div>
        <div class="col-lg-6">
            <br>
            <div class="header-bottom bn">

                <form>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="full-name">পুরো নাম</label>
                                <input type="text" class="form-control" id="full-name" placeholder="পুরো নাম লিখুন">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="phone">ফোন নম্বর</label>
                                <input type="text" class="form-control" id="phone" placeholder="ফোন নম্বর লিখুন">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="form-group">
                                <label for="email">ইমেল</label>
                                <input type="email" class="form-control" id="email" placeholder="ইমেইল প্রদান করুন">
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="form-group">
                                <label for="date">তারিখ</label>
                                <input type="date" class="form-control" id="date">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="date">প্যাকেজ</label>
                        <select class="form-control">
                            <option selected>Choose package...</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <label for="note">বুকিং নোট</label>
                        <textarea name="" class="form-control" id="note" cols="30" rows="3" placeholder="বুকিং নোট"></textarea>
                    </div>
                    <button type="submit" class="btn btn-success btn-block btn-search">
                        এখনই বুক করুন
                    </button>
                </form>
            </div>
        </div>
    </div>
    
</div>