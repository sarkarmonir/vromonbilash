<footer id="footer">
    <div class="container bn">
      <div class="row">
        <div class="col-lg-3">
            <h3>আমাদের সম্পর্কে</h3>
            <img width="70" src="{{ url('assets/web/images/vromon-logo.png') }}" alt="">
            <p>বাংলা সাহিত্যের সম্রাট কিংবদন্তী লেখক হুমায়ূন আহমেদের নুহাশ পল্লী ও দারুচিনি দ্বীপের “সমুদ্র বিলাসে ” আপনাকে স্বাগতম</p>
        </div>
        <div class="col-lg-3">
            <h3>ভ্রমণ</h3>
            <ul class="bn">
                <li><a href="{{url('nuhashpolli')}}">নুহাশ পল্লী</a></li>
                <li><a href="{{url('somudrobilash')}}">সমুদ্র বিলাস</a></li>
                <li><a href="{{url('resort')}}">সেন্ট মার্টিন</a></li>
                <li><a href="{{url('package')}}">প্যাকেজ ট্যুর</a></li>
            </ul>
        </div>
        <div class="col-lg-3">
            <h3>ভ্রমণ বিলাস</h3>
            <ul class="bn">
                <li><a href="{{url('about')}}">আমাদের সম্পর্কে</a></li>
                <li><a href="{{url('faq')}}">জিজ্ঞাসা</a></li>
                <li><a href="{{url('team')}}">আমাদের টিম</a></li>
                <li><a href="{{url('contact')}}">যোগাযোগ</a></li>
            </ul>
        </div>
        <div class="col-lg-3">
            <h3>আমাদের</h3>
            <ul class="social-link">
                <li><a href="#"><i class="fab fa-facebook-square  fa-2x"></i></a></li>
                <li><a href="#"><i class="fab fa-google-plus-square fa-2x"></i></a></li>
                <li><a href="#"><i class="fab fa-youtube-square fa-2x"></i></a></li>
                <li><a href="#"><i class="fab fa-twitter-square fa-2x"></i></a></li>
                <li><a href="#"><i class="fab fa-linkedin fa-2x"></i></a></li>
            </ul>
        </div>
      </div>
    </div>
</footer>

<div class="footer-bottom bn">
    <div class="container">
        <div class="row">
            <div class="col-lg-6"><p>কপিরাইট © ২০১৯ - সমস্ত অধিকার সংরক্ষিত</p></div>
            <div class="col-lg-6"><p class="float-right">প্রযুক্তিগত সহায়তা এবং উন্নয়নে <a href="http://cloudnextltd.com/" target="blank"> ক্লাউড নেক্সট জেনারেশন লিমিটেড</a></p></div>
        </div>
    </div>
</div>

<a href="#">
    <img class="scrollup" src="{{ url('assets/web/images/arrow-up.png') }}" alt="">
</a>