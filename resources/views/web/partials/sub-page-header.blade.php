<section class="slider sub-page">
    <div class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="bn">{{ isset($page_title) ? $page_title : 'no title' }}</h2>           
                </div>
            </div>
        </div>
    </div>
  </section>