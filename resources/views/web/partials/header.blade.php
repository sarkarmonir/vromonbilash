<header id="header">
    {{-- <div class="top-nav">
        <div class="container">
            <div class="float-left">
            <p class="d-none d-sm-block"><i class="fa fa-phone-square"></i> 0123 456 789</p>
            </div>
            <div class="float-right">
            <ul class="top-menu">
                <li><a href="help.html"><i class="fa fa-info-circle"></i> Help</a></li>
                <li><a href="my-account.html"><i class="fa fa-user-circle"></i> My Account</a></li>
                <li><a href="login.html"><i class="fa fa-sign-in-alt"></i> Login</a></li>
                <li><a href="registration.html"><i class="fa fa-user-plus"></i> Registration</a></li>
            </ul>
            </div>
        </div>
    </div> --}}
    <div class="d-block d-sm-none res-point">
        <i class="fa fa-bars"></i>
    </div>
    <div class="navigation sticker">
        <div class="container">    
            <div class="logo">
                <a href="{{url('/')}}">
                    <img src="{{ url('assets/web/images/vromon-logo.png') }}" alt="">
                </a>
            </div>
        </div>
        <div class="main-nav">
        <div class="overly"></div>
        <div class="container">
            <ul class="bn">
                <li><a href="{{url('/')}}"><i class="fa fa-home"></i> হোম</a></li>
                <li><a href="{{url('about')}}">আমাদের সম্পর্কে</a></li>
                <li><a href="{{url('nuhashpolli')}}">নুহাশ পল্লী</a></li>
                <li><a href="{{url('somudrobilash')}}">সমুদ্র বিলাস</a></li>
                <li><a href="{{url('resort')}}">সেন্ট মার্টিন</a></li>
                <li><a href="{{url('package')}}">প্যাকেজ ট্যুর</a></li>
                <li><a href="{{url('faq')}}"><i class="fa fa-question-circle"></i> জিজ্ঞাসা</a></li>
                <li><a href="{{url('team')}}">আমাদের টিম</a></li>
                <li><a href="{{url('contact')}}">যোগাযোগ</a></li>
            </ul>
        </div>
        </div>
    </div>
    {{-- <div class="container">
        <br>
        <div class="row">
            <div class="col-lg-9"></div>
            <div class="col-lg-3">
                <div class="hol-line animated infinite pulse bn">
                    <img src="{{ url('assets/web/images/vromon-logo.png') }}" alt="">
                    <h2><i class="fas fa-phone fa-rotate-90"></i> ০১৯১১৯২০৬৬৬</h2>
                </div>
            </div>
        </div>
    </div> --}}
</header>