<?php

namespace App\Http\Controllers\Web;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class HomeController extends BaseController
{
    public function index(){
        $data['title'] = "ভ্রমণ বিলাস";
        return view('web.pages.home', $data);
    }
    public function about(){
        $data['title'] = "ভ্রমণ বিলাস - আমাদের সম্পর্কে";
        $data['page_title'] = "আমাদের সম্পর্কে";
        return view('web.pages.nuhashpolli', $data);
    }
    public function nuhashpolli(){
        $data['title'] = "ভ্রমণ বিলাস - নুহাশ পল্লী";
        $data['page_title'] = "নুহাশ পল্লী";
        return view('web.pages.nuhashpolli', $data);
    }
    public function somudrobilash(){
        $data['title'] = "ভ্রমণ বিলাস - সমুদ্র বিলাস";
        $data['page_title'] = "সমুদ্র বিলাস";
        return view('web.pages.somudrobilash', $data);
    }
    public function resort(){
        $data['title'] = "ভ্রমণ বিলাস - রিসোর্ট";
        $data['page_title'] = "রিসোর্ট";
        return view('web.pages.resort', $data);
    }
    public function package(){
        $data['title'] = "ভ্রমণ বিলাস - প্যাকেজ ট্যুর";
        $data['page_title'] = "প্যাকেজ ট্যুর";
        return view('web.pages.package', $data);
    }
    public function resortdetails(){
        $data['title'] = "ভ্রমণ বিলাস - রিসোর্ট";
        $data['page_title'] = "রিসোর্ট";
        return view('web.pages.resortdetails', $data);
    }
    public function faq(){
        $data['title'] = "ভ্রমণ বিলাস - জিজ্ঞাসা";
        $data['page_title'] = "জিজ্ঞাসা";
        return view('web.pages.faq', $data);
    }
    public function team(){
        $data['title'] = "ভ্রমণ বিলাস - আমাদের টিম";
        $data['page_title'] = "আমাদের টিম";
        return view('web.pages.team', $data);
    }
    public function contact(){
        $data['title'] = "ভ্রমণ বিলাস - যোগাযোগ";
        $data['page_title'] = "যোগাযোগ";
        return view('web.pages.contact', $data);
    }
}
