<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


// home
Route::get('/', 'Web\HomeController@index');

Route::get('/about', 'Web\HomeController@about');
Route::get('/nuhashpolli', 'Web\HomeController@nuhashpolli');
Route::get('/somudrobilash', 'Web\HomeController@somudrobilash');
Route::get('/resort', 'Web\HomeController@resort');
Route::get('/package', 'Web\HomeController@package');
Route::get('/resortdetails', 'Web\HomeController@resortdetails');
Route::get('/faq', 'Web\HomeController@faq');
Route::get('/team', 'Web\HomeController@team');
Route::get('/contact', 'Web\HomeController@contact');
